#ifndef AVTOIO_H
#define AVTOIO_H

#include <QtCore>

class AvtoItem {
public:
    QString producer;
    QString vendorCode;

    QHash <QString, QString> crossData;

    static AvtoItem fromCsv(const QString & csv);
};

class AvtoIO
{
public:
    static QList <AvtoItem> fromCsv(const QString &path);
    static void toCsv(const AvtoItem & item, const QString & path);
    static void logToFile(const QString &message,
                          const QString & path = QCoreApplication::applicationDirPath() + "/log.txt");
};

#endif // AVTOIO_H
