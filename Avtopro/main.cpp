#include <QCoreApplication>

#include <avtoio.h>
#include <avtowebclient.h>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    const auto path = QCoreApplication::applicationDirPath() + "/avtopro.csv";
    auto items = AvtoIO::fromCsv(path);

    AvtoWebClient avto;
    avto.updateCookie();

    for (auto & item : items) {

        avto.search(item);

        if (!item.crossData.isEmpty()) {
            AvtoIO::toCsv(item, QCoreApplication::applicationDirPath() + "/cross.csv");
        }

    }

    return a.exec();
}
