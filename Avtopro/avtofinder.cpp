#include "avtofinder.h"

std::tuple <bool, QString> AvtoFinder::findCrossData(AvtoItem &item, const QByteArray & html)
{
    static const QRegularExpression regex("<tr.*?pl__maker-name.*?\\/tr>");
    auto match = regex.globalMatch(html);

    while (match.hasNext()) {
        // pl__maker-name - producer id
        // part - crossVendorCode id
        const auto crossRaw = match.next().captured();
        const auto [producer, crossCode] = AvtoFinder::findProducerAndCrossVendorCode(crossRaw);

        if (!producer.isEmpty() && !crossCode.isEmpty() && crossCode != item.vendorCode) {
            item.crossData[crossCode] = producer;
        }

    }

    static const QRegularExpression nextPageRegex("data-next-page=\"(.*?)\"");
    const auto isNeedNextPage = nextPageRegex.match(html).captured(1).toInt() > 0;

    static const QRegularExpression descriptorRegex("data-next-page=\"(.*?)\"");
    const auto descriptor = descriptorRegex.match(html).captured(1).replace("&quot;", "\"");

    return {isNeedNextPage, descriptor};
}

std::tuple <QString, QString>  AvtoFinder::findProducerAndCrossVendorCode(const QString &html)
{
    static const QRegularExpression regex(
                "<span.*?pl__maker-name.*?>(.*?)<\\/span>.*?<a.*?part.*?>(.*?)<\\/a>"
    );

    const auto match = regex.match(html);
    return {match.captured(1), match.captured(2)};
}
