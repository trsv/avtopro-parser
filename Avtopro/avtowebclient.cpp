#include "avtowebclient.h"

AvtoWebClient::AvtoWebClient(QObject * parent)
    : QObject (parent)
    , m_network(this)
{
    m_network.setCookieJar(&m_cookies);
}

void AvtoWebClient::updateCookie()
{
    auto reply = sendRequest("GET", QNetworkRequest(QUrl("https://avto.pro/")));
    reply->deleteLater();
}

void AvtoWebClient::search(AvtoItem &item)
{
    qDebug() << "[Avto WebClient] Searching" << item.vendorCode;

    QNetworkRequest request(QUrl("https://avto.pro/api/v1/search/query"));
    request.setRawHeader("Accept", "text/plain");
    request.setRawHeader("Origin", "https://avto.pro");
    request.setRawHeader("X-Requested-With", "XMLHttpRequest");
    request.setRawHeader("Content-type", "application/json");
    request.setRawHeader("Referer", "https://avto.pro/");

    auto data = item.vendorCode;
    if (!item.producer.isEmpty()) {
        data.prepend(item.producer + ";");
    }

    QJsonObject jsonArgs;
    jsonArgs["Query"] = data;

    auto reply = sendRequest("PUT", request, QJsonDocument(jsonArgs).toJson(QJsonDocument::Compact));

    if (reply->error() == QNetworkReply::NoError) {

        QJsonParseError error;
        const auto json = QJsonDocument::fromJson(reply->readAll(), &error);

        if (error.error == QJsonParseError::NoError) {

            const auto response = json.object();
            const auto suggestions = response["Suggestions"].toArray();

            if (!suggestions.isEmpty()) {

                QString url;
                for (const auto & suggestion : suggestions) {
                    const auto producer = suggestion["FoundPart"].toObject()["Part"]
                            .toObject()["Brand"].toObject()["Name"].toString();

                    if (producer.toLower() == item.producer.toLower()) {
                        url = suggestion["Uri"].toString();
                        break;
                    }
                }

                if (!url.isEmpty()) {
                    searchCross(item, url);
                } else {
                    const auto message = QString("[WARNING] No result for %1 %2")
                            .arg(item.producer, item.vendorCode);
                    qWarning() << "[Avto WebClient]" << message;
                    AvtoIO::logToFile(message);
                }

            }

        } else {
            const auto message = QString("[ERROR] Json parsing response failed %1 %2 %3")
                    .arg(item.producer, item.vendorCode, error.errorString());
            qWarning() << "[Avto WebClient] " << message;
            AvtoIO::logToFile(message);
        }
    } else {
        const auto message = QString("[ERROR] Searching failed %1 %2 %3")
                .arg(item.producer, item.vendorCode, reply->errorString());
        qWarning() << "[Avto WebClient]" << message;
        AvtoIO::logToFile(message);
    }

    reply->deleteLater();
}

QNetworkReply *AvtoWebClient::sendRequest(const QByteArray &method, const QNetworkRequest &request, const QByteArray &data)
{
    auto reply = m_network.sendCustomRequest(request, method, data);

    QEventLoop loop;
    connect(reply, &QNetworkReply::finished, &loop, &QEventLoop::quit);
    loop.exec();

    const auto newCookies =
            qvariant_cast <QList <QNetworkCookie>> (reply->header(QNetworkRequest::SetCookieHeader));

    m_cookies.setCookiesFromUrl(newCookies, request.url());

    return reply;
}

void AvtoWebClient::searchCross(AvtoItem &item, const QString & url)
{
    qDebug() << "[Avto WebClient] Search cross data" << item.vendorCode;

    QNetworkRequest request(QUrl("https://avto.pro" + url));
    request.setRawHeader("Referer", "https://avto.pro/");
    request.setRawHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9," \
                         "image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3");
    request.setAttribute(QNetworkRequest::FollowRedirectsAttribute, true);

    auto reply = sendRequest("GET", request);

    if (reply->error() == QNetworkReply::NoError) {

        const auto [isNeedNextPage, descriptor] = AvtoFinder::findCrossData(item, reply->readAll());

        if (isNeedNextPage && !descriptor.isEmpty()) {
            searchCrossNextPage(item, descriptor);
        }

    } else {
        const auto message = QString("[ERROR] Search cross data failed %1 %2 %3")
                .arg(item.producer, item.vendorCode, reply->errorString());
        qWarning() << "[Avto WebClient]" << message;
        AvtoIO::logToFile(message);
    }

    reply->deleteLater();
}

void AvtoWebClient::searchCrossNextPage(AvtoItem &item, const QString &descriptor)
{
    const auto descriptorJson = QJsonDocument::fromJson(descriptor.toUtf8());

    const auto code = descriptorJson[""].toString();
    const auto manPath = descriptorJson[""].toString();
    const auto isMobile = descriptorJson[""].toVariant().toString();

    QUrlQuery query;
    query.addQueryItem("code", code);
    query.addQueryItem("manpath", manPath);
    query.addQueryItem("ismobile", isMobile);
    query.addQueryItem("autopartsListContinuationToken", descriptor);

    QUrl url;
    url.setUrl("https://avto.pro/system/parts/catalogparts-ajax/.aspx");
    url.setQuery(query);

    QNetworkRequest request(url);
    request.setRawHeader("Content-type", "application/x-www-form-urlencoded");
    request.setRawHeader("Accept", "*/*");
    request.setRawHeader("X-Requested-With", "XMLHttpRequest");

    auto reply = sendRequest("GET", request);

    if (reply->error() == QNetworkReply::NoError) {

        const auto [isNeedNextPage, nextDescriptor] =
                AvtoFinder::findCrossData(item, reply->readAll());

        if (isNeedNextPage && !nextDescriptor.isEmpty()) {
            searchCrossNextPage(item, nextDescriptor);
        }

    } else {
        const auto message = QString("[ERROR] Searching cross data pagination failed %1 %2 %3")
                .arg(item.producer, item.vendorCode, reply->errorString());
        qWarning() << "[Avto Webclient]" << message;
        AvtoIO::logToFile(message);
    }

    reply->deleteLater();

}

