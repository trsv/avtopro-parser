#ifndef AVTOWEBCLIENT_H
#define AVTOWEBCLIENT_H

#include <avtoio.h>
#include <avtofinder.h>

#include <QtCore>
#include <QtNetwork>

class AvtoWebClient : public QObject
{
    Q_OBJECT

public:
    explicit AvtoWebClient(QObject * parent = nullptr);

public:
    void updateCookie();
    void search(AvtoItem & item);

private slots:
    QNetworkReply * sendRequest(const QByteArray & method,  const QNetworkRequest & request,
                                const QByteArray & data = QByteArray());

    void searchCross(AvtoItem & item, const QString & url);
    void searchCrossNextPage(AvtoItem & item, const QString & descriptor);

private:
    QNetworkCookieJar m_cookies;
    QNetworkAccessManager m_network;
};

#endif // AVTOWEBCLIENT_H
