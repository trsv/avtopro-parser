#ifndef AVTOFINDER_H
#define AVTOFINDER_H

#include <avtoio.h>
#include <tuple>

class AvtoFinder
{
public:
    static std::tuple<bool, QString> findCrossData(AvtoItem &item, const QByteArray & html);

private:
    static std::tuple <QString, QString> findProducerAndCrossVendorCode(const QString & html);
};

#endif // AVTOFINDER_H
