#include "avtoio.h"

QList<AvtoItem> AvtoIO::fromCsv(const QString & path)
{
    qDebug() << "[Avto IO] Reading file" << path;

    QFile file(path);

    if (!file.open(QIODevice::ReadOnly)) {
        qWarning() << "[Avto IO] File reading failed" << path;
        return QList <AvtoItem> ();
    }

    QSet <QString> insertedItems;
    QList <AvtoItem> list;

    while (!file.atEnd()) {
        const auto item = AvtoItem::fromCsv(file.readLine());
        const auto itemKey = item.producer + item.vendorCode;

        if (!insertedItems.contains(itemKey)) {
            insertedItems << itemKey;
            list << item;
        }
    }

    file.close();
    return list;
}

void AvtoIO::toCsv(const AvtoItem &item, const QString & path)
{
    qDebug() << "[Avto IO] Write cross csv data" << item.vendorCode;

    QByteArray crossDataCsv;
    for (auto it = item.crossData.cbegin(); it != item.crossData.cend(); ++it) {
        crossDataCsv.append(item.producer + ";");
        crossDataCsv.append(item.vendorCode + ";");
        crossDataCsv.append(it.value() + ";");
        crossDataCsv.append(it.key());
        crossDataCsv.append("\n");
    }

    QFile file(path);
    if (file.open(QIODevice::WriteOnly | QIODevice::Append)) {
        file.write(crossDataCsv);
        file.close();
    }
}

void AvtoIO::logToFile(const QString &message, const QString &path)
{
    QFile file(path);
    if (file.open(QIODevice::WriteOnly | QIODevice::Append)) {
        file.write(message.toUtf8() + "\n");
        file.close();
    } else {
        qWarning() << "[Avto IO] Failed to write log file";
    }
}


AvtoItem AvtoItem::fromCsv(const QString &csv)
{
    AvtoItem item;

    const auto data = csv.split(";");
    item.producer = data.at(0).trimmed();
    item.vendorCode = data.at(1).trimmed();

    return item;
}
